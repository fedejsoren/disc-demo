
const facetsData = [
    {
        name: "Brand",
        options: ["Discmania", "Innova", "Dynamic Discs"],
        amount: 1,
    },
    {
        name: "Color",
        options: ["Red", "Blue", "Purple", "Yellow"],
        amount: 2,
    },
    {
        name: "Speed",
        options: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        "amount": 26
    },
];

const productData = [
    {
        "id": 1,
        "name": "Mentor",
        [facetsData[0].name]: facetsData[0].options[0],
        [facetsData[1].name]: facetsData[1].options[0],
        [facetsData[2].name]: facetsData[2].options[0],
        "price": 17,
        "image": "https://www.discgolfstore.de/media/image/product/22063/lg/active-premium-mentor-174g-pink.jpg"
    },
    {
        "id": 2,
        "name": "Essence",
        [facetsData[0].name]: facetsData[0].options[0],
        [facetsData[1].name]: facetsData[1].options[0],
        [facetsData[2].name]: facetsData[2].options[1],
        "price": 15,
        "image": "https://www.discgolfstore.de/media/image/product/18063/md/neo-essence-173g-pink.jpg"
    },
    {
        "id": 3,
        "name": "Mentor",
        [facetsData[0].name]: facetsData[0].options[0],
        [facetsData[1].name]: facetsData[1].options[1],
        [facetsData[2].name]: facetsData[2].options[0],
        "price": 12,
        "image": "https://www.discgolfstore.de/media/image/product/22061/md/active-premium-mentor-174g-gelb.jpg"
    },
    {
        "id": 4,
        "name": "Essence",
        [facetsData[0].name]: facetsData[0].options[0],
        [facetsData[1].name]: facetsData[1].options[1],
        [facetsData[2].name]: facetsData[2].options[1],
        "price": 15,
        "image": "https://www.discgolfstore.de/media/image/product/18063/md/neo-essence-173g-pink.jpg"
    },
    {
        "id": 5,
        "name": "Mentor",
        [facetsData[0].name]: facetsData[0].options[1],
        [facetsData[1].name]: facetsData[1].options[0],
        [facetsData[2].name]: facetsData[2].options[0],
        "price": 15,
        "image": "https://www.discgolfstore.de/media/image/product/22063/lg/active-premium-mentor-174g-pink.jpg"
    },
    {
        "id": 6,
        "name": "Essence",
        [facetsData[0].name]: facetsData[0].options[1],
        [facetsData[1].name]: facetsData[1].options[0],
        [facetsData[2].name]: facetsData[2].options[1],
        "price": 22,
        "image": "https://www.discgolfstore.de/media/image/product/18063/md/neo-essence-173g-pink.jpg"
    },
    {
        "id": 7,
        "name": "Mentor",
        [facetsData[0].name]: facetsData[0].options[1],
        [facetsData[1].name]: facetsData[1].options[1],
        [facetsData[2].name]: facetsData[2].options[1],
        "price": 22.2,
        "image": "https://www.discgolfstore.de/media/image/product/22061/md/active-premium-mentor-174g-gelb.jpg"
    },
    {
        "id": 8,
        "name": "Essence",
        [facetsData[0].name]: facetsData[0].options[2],
        [facetsData[1].name]: facetsData[1].options[2],
        [facetsData[2].name]: facetsData[2].options[2],
        "price": 17.2,
        "image": "https://www.discgolfstore.de/media/image/product/18063/md/neo-essence-173g-pink.jpg"
    },
]

function getFacetsData() {
    return facetsData;
}

function getProductData() {
    return productData;
}

export {getFacetsData, getProductData};
