import React, { useState } from 'react';

function Facet({ name, amount, options, addFacetOption, removeFacetOption, checkFacetIsSelected }) {
    
    const [isOpen, setIsOpen] = useState(true);
    const [symbol, changeSymbol] = useState("v")


    return (
        <div className="facet">
            <div className='facet-body'>
            <p
                id = {name + "-facet"}
                type="checkbox"
                className="facet-open"
                value={name}
                defaultChecked={isOpen}
                onClick={() => { setIsOpen(!isOpen); if(!isOpen) { changeSymbol("v")} else { changeSymbol(">")} }}
            >{symbol}</p>
            <p className="facet-name">{name}</p>
            <p className="facet-amount">({amount})</p>
            </div>
            {isOpen &&
            <div className='facet-options'>
                    {options.map((option) => (
                        <div className='flex' key={option}>
                            <input
                             type="checkbox"
                             value={name}
                             defaultChecked={() => checkFacetIsSelected(name, option)}
                             onChange={() => checkFacetIsSelected(name, option) ? removeFacetOption(name, option) : addFacetOption(name, option)}
                            />
                            <p className='facet-option' key={option}>{option}</p>
                        </div>    
                    ))}
            </div>
            }
        </div>
    );
}

export default Facet;