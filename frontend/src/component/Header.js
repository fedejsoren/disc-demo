import React from 'react'
import TextInput from './TextInput'

function header() {
    return (
      <header className="search" >
          <TextInput />
          <button className="button">Search</button>
      </header>
    );
}
export default header;