import React from 'react'
import Product from "./ProductTile";

function products({products}) {
    return(
        <div className="products">
            {products.map((product) => {
                return (Product(product))
            })}
        </div>
    )
}

export default products;