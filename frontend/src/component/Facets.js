import React from 'react'
import Facet from './Facet'

function Facets ({ facets, addFacetOption, removeFacetOption, checkFacetIsSelected}) {
    return (
        <div className="facets" >
            {facets.map((facet) => {
                return (
                    <Facet
                        key={facet.name}
                        name={facet.name}
                        amount={facet.amount}
                        options={facet.options}
                        addFacetOption={addFacetOption}
                        removeFacetOption={removeFacetOption}
                        checkFacetIsSelected={checkFacetIsSelected}
                    />)
            })}
        </div>
    )
}

export default Facets;