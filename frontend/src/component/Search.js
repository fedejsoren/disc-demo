import './Search.css';
import React, { useEffect } from 'react';
import {useState} from 'react'

import Header from './Header'
import Facets from './Facets'
import Products from './Products'
import {getProductData} from '../lib/Backend'
import {getFacetsData} from '../lib/Backend'


function Search() {

    const [facets, setFacets] = useState(getFacetsData());
    const [selectFacetOptions, setSelectedFacetOptions] = useState({
        brand: [],
        size: [],
        color: []
    });

    /* const [selectedProducts, setP] = useState([]);

    useEffect(() => {
        console.log("selectedFaceOptions changed");
    }, [selectFacetOptions]); */

    const checkFacetIsSelected = (facetName, option) => {
     return selectFacetOptions[facetName].includes(option);
    }

    const addFacetOption = (facetName, option) => {
        const options = selectFacetOptions[facetName];
        if (!checkFacetIsSelected(facetName, option)) {
            options.push(option);
            setSelectedFacetOptions({
                ...selectFacetOptions,
                [facetName]: options
            });
        }

    };

    const removeFacetOption = (facetName, option) => {
        let options = selectFacetOptions[facetName];    
        if (checkFacetIsSelected(facetName, option)) {
        options = options.filter(value => value !== option);
            setSelectedFacetOptions({
                ...selectFacetOptions,
                [facetName]: options
            });
        }
    };

    return (
        <div className="app">
            <Header className="search"/>
            <div className="row">
                <Facets
                    className="facets"
                    facets={facets}
                    addFacetOption={addFacetOption}
                    removeFacetOption={removeFacetOption}
                    checkFacetIsSelected={checkFacetIsSelected}
                />
                <Products className="products" products={getProductData()}/>
            </div>
        </div>
    )
}

export {Search};
