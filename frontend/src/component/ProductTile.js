import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

function Product({id, name, price, image}) {
    return (
        <Link to="/product">
            <div className="product" key={id}>
                <img src={image} className="thumbnail" alt={name}/>
                <p>{name}</p>
                <p>{price}€</p>
            </div>
        </Link>
    );
}

export default Product;