import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

/**
 * Import all page components here
 */
import {Search} from './component/Search';
import {ProductDetail} from './component/ProductDetail'

const App = () => {
  return (
    <Switch>
      <Route path='/'>
        <Search/>
      </Route>
      <Route path='/product'>
        <ProductDetail/>
      </Route>
    </Switch>
  );
}

export default App;